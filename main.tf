#créer une public IP
resource "azurerm_public_ip" "publicIP2" {
  name                = "publicIP2"
  location            = "${var.location}"
  resource_group_name = "${var.name}"
  allocation_method   = "${var.alloc_method}"
  domain_name_label   = "tp-springboot"
}
#créer un NSG
resource "azurerm_network_security_group" "mySecurityGroup2" {
  name                = "mySG2"
  location            = "${var.location}"
  resource_group_name = "${var.name}"

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "HTTP"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Springboot"
    priority                   = 1003
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8080"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
data "azurerm_subnet" "mySubnet" {
  name                 = "testSubnet"
  virtual_network_name = "testVnet"
  resource_group_name  = "grosgib2"
}
resource "azurerm_network_interface" "myNetworkInterface2" {
  name                = "myNetworkInterface2"
  location            = "${var.location}"
  resource_group_name = "${var.name}"
  network_security_group_id = "${azurerm_network_security_group.mySecurityGroup2.id}"

  ip_configuration {
    name                          = "internal"
    subnet_id                     = "${data.azurerm_subnet.mySubnet.id}"
    private_ip_address_allocation = "Static"
    private_ip_address = "10.0.2.5"
    public_ip_address_id = "${azurerm_public_ip.publicIP2.id}"
  }
}
resource "azurerm_virtual_machine" "main" {
  name                  = "vm2"
  location              = "${var.location}"
  resource_group_name   = "${var.name}"
  network_interface_ids = ["${azurerm_network_interface.myNetworkInterface2.id}"]
  vm_size               = "${var.vm_size}"

  storage_image_reference {
    publisher = "Openlogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  storage_os_disk {
    name              = "mydisk2"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "grosgib2"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
  }
  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
        path="/home/stage/.ssh/authorized_keys"
        key_data = "${var.key_data}"
    }
  }
}